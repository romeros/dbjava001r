//ROMERO MARTINEZ BRANDON


package modelo;
import javax.swing.JOptionPane;
/**
 *
 * @author efrai
 */
public class TestDbProducto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        dbProducto db= new dbProducto();
        Productos pro= new Productos();
        if(db.conectar()){
            System.out.println("Fue posible conectar");
            
            pro.setCodigo("1000");
            pro.setNombre("Atun");
            pro.setPrecio(10.50f);
            pro.setStatus(0);
            
            try{
                db.insertar(pro);
                JOptionPane.showMessageDialog(null, "Se agrego con exito");
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "Surgio un error" + e.getMessage());
            }
            
            pro.setIdProductos(1);
            pro.setNombre("Jabon Ariel 1kg");
            pro.setCodigo("201");
            pro.setFecha("2024-06-25");
            pro.setPrecio(45.40f);
            try{
                db.actualizar(pro);
                JOptionPane.showMessageDialog(null,"Se Actualizo con exito");
            }catch(Exception e){
                JOptionPane.showMessageDialog(null,"surgio un error" + e.getMessage());
            }
        }
    }
    
}

//ROMERO MARTINEZ BRANDON


package modelo;
import java.util.ArrayList;
/**
 *
 * @author efrai
 */
public class dbProducto extends dbManejador implements Persistencia {
    public dbProducto(){
    super();
}
    @Override
    public void insertar(Object object) throws Exception {
    Productos pro = new Productos();
    pro = (Productos)object;
    String consulta="";
    consulta = "insert into productos (codigo,nombre,fecha,precio,status)" + "values(?,?,?,?,?)";
    if(this.conectar()){
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1,pro.getCodigo());
        this.sqlConsulta.setString(2,pro.getNombre());
        this.sqlConsulta.setString(3,"2024-06-20");
        this.sqlConsulta.setFloat(4,pro.getPrecio());
        this.sqlConsulta.setInt(5,pro.getStatus());
        this.sqlConsulta.executeUpdate();
        this.desconectar();
    }    
    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = new Productos();
    pro = (Productos)object;
    String consulta="";
    consulta = "update productos set codigo = ?,nombre = ?,fecha = ?,precio=?" + "where idProductos = ? and status = 0";
    if(this.conectar()){
    this.sqlConsulta = this.conexion.prepareStatement(consulta);
    this.sqlConsulta.setString(1,pro.getCodigo());
    this.sqlConsulta.setString(2,pro.getNombre());
    this.sqlConsulta.setString(3,pro.getFecha());
    this.sqlConsulta.setFloat(4,pro.getPrecio());
    this.sqlConsulta.setInt(5,pro.getIdProductos());
        this.sqlConsulta.executeUpdate();
        this.desconectar();
    }      
    }

    @Override
    public void habilitar(Object object) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void desahabilitar(Object object) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean siExiste(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList lista() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}

